# resolv

#### Table of Contents

1. [Description](#description)
1. [Beginning with resolv](#beginning-with-resolv)
1. [Usage - Configuration options and additional functionality](#usage)

## Description

This is a very simple module that will take in an array of IP's and put them into resolv.conf

### Beginning with resolv

Defaulted to 8.8.8.8, 8.8.4.4, you can pass in an array of IP's only

## Usage

```puppet
class {'resolv': 
  ips => ['1.2.3.4', '5.6.7.8'],
}
```
