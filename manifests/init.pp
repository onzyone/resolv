# Class: resolv
# ===========================
#
# Full description of class resolv here.
##
# Variables
# ----------
#
# $ips (this has to be an array of ips, the module will fail if you only pass 1 ip)
#
# Examples
# --------
#
# @example
#    class { 'resolv':
#      ips => [ '1.2.3.4', '5.6.7.8' ],
#    }
#
# Authors
# -------
#
# Jason Chinsen <jaosn.chinsen@gmail.com>
#
# Copyright
# ---------
#
# Copyright 2016 Jason Chinsen, unless otherwise noted.
#
# The module must do the following;
# -     Using the Puppet 4 data typing framework, validate that the param is an array of IP addresses
#       (They _must_ be IP addresses, not strings or ints, and we don’t want stdlib to be used to implement this)
# -     Write the file out using the Puppet EPP template language
# -     The EPP Template must also validate the array of IP addresses are valid IP addresses and not just strings / ints in an array


class resolv (
  Array[Pattern[/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/]] $ips = $resolv::params::ips
)inherits resolv::params{
  file {'/etc/resolv.conf':
    ensure  => file,
    mode    => '0644',
    owner   => 'root',
    content => epp('resolv/etc/resolv.conf.epp', {ips => $ips})
  }
}
