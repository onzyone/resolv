# class resolv::params
# Set the defalut paramters for the resolv module
class resolv::params {
  $source_base  = "puppet:///modules/${module_name}/"
  $ips          = ['8.8.8.8', '8.8.4.4']
}